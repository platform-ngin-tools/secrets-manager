# The Secrets-manager application

## About
A simple secret manager system, but provide an API to access secrets data.

## Description
You add a namespace, each namespace has a apllication and any applications has a many number of the secrets.
Each namespace has via API access at the secrets data.

## Installation
just use: ```pip install -r requirements.txt```

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
You can to do anything to help this project, because hin is in initial state.

## Authors and acknowledgment
Just me, yet.

## License
GPLv3

## Project status
Maturit level:
 - 0 (We don't have anythink)

## Initial Steps
   * For Maturit level - 1:
     - Create a simple API as a CRUD to secrets (use fastAPI). 
     - Create a web interface (maybe can use anythink as reactjs or vanila JavaScript)